package com.elihu.myapplication;

public class LoadData implements java.io.Serializable {

    public static Profile LoadData(String who) {
        if (who.equalsIgnoreCase("Elihu")) {
            Profile elihu = new Profile("Elihu", "Del Valle", 29, "elihu-@hotmail.com", "0434-218-402");

            //Adding Technologies
            elihu.addTechnology("Java");
            elihu.addTechnology("Python");
            elihu.addTechnology("R");
            elihu.addTechnology("SQL (Oracle & PostgreSQL)");
            elihu.addTechnology("MongoDB");
            elihu.addTechnology("NodeJS");
            elihu.addTechnology("AngularJS");
            elihu.addTechnology("Django");
            elihu.addTechnology("Bootstrap");
            elihu.addTechnology("HMTL/CSS/Javascript");
            elihu.addTechnology("Git");
            elihu.addTechnology("Android");
            elihu.addTechnology("Unix");
            elihu.addTechnology("Bash");
            elihu.addTechnology("Java");
            elihu.addTechnology("Jenkins");
            elihu.addTechnology("Selenium");

            //Adding Languages
            Language spanish = new Language("Spanish", "Native");
            Language english = new Language("English", "PTE Score:78, TOELF IBT Score:100");
            Language french = new Language("French", "Intermediate");
            elihu.addLanguage(spanish);
            elihu.addLanguage(english);
            elihu.addLanguage(french);

            //Adding Referees
            Referee oscar = new Referee("Oscar Ruiz", "Managing Director", "oscar.ruiz@axicor.com", "423-323-644", "11/530 Little Collins St, Melbourne VIC 3000");
            Referee scott = new Referee("Scott Mann", "Lecturer", "s.mann@latrobe.edu.au", "394-792-202", "Plenty Rd & Kingsbury Dr, Bundoora VIC 3086");
            elihu.addReferee(oscar);
            elihu.addReferee(scott);

            //Adding Education
            Education bachelors = new Education("Universidad del Valle de Mexico", "Indusrial and Systems Engineering", "Saltillo/Mexico", "2007", "2012");
            Education masters = new Education("La Trobe University", "Master of Information Technology", "Melbourne/Australia", "2016", "2018");
            Education exchange = new Education("Universidad Europea de Madrid", "Exchange (Industrial and Systems Engineering)", "Madrid/Spain", "2010", "2010");
            elihu.addEducation(masters);
            elihu.addEducation(bachelors);
            elihu.addEducation(exchange);

            //Adding Volunteering
            Experience brainStem = new Experience("BrainSTEM", "2017-current", "Salesforce Administrator");
            brainStem.addDuty("Helping this NGO with customization of Salesforce for their needs, general administration, data and maintenance of the platform");
            elihu.addVolunteering(brainStem);

            //Adding Jobs
            Experience axicor = new Experience("Axicor", "2018", "Data Scientist");
            axicor.addDuty("Development of algorithms for prediction and identification purposes using R, Python");
            axicor.addDuty("Data extraction from databases using postgreSQL for cleaning and preparation");
            axicor.addDuty("Use of Machine Learning techniques as Decision Trees, Clustering techniques ");
            axicor.addDuty("Data visualization using Tableau");
            axicor.addDuty("Development of web based applications using Python/ Django framework");
            elihu.addJob(axicor);

            Experience telstra = new Experience("Telstra", "2018", "Software Engineer Intern");
            telstra.addDuty("Member of the Software Experience Engineering Team for the global enterprise product engineering group");
            telstra.addDuty("Development of applications using voice assistant Echo’s Amazon (Alexa) on local servers using NodeJS and MongoDB");
            telstra.addDuty("Development of test automation scripts (Java) using tools as Selenium and Jenkins to compile with the CI/CD methodology");
            telstra.addDuty("Design of dashboards using API’s as Google Charts and Frameworks as React and Material dashboards");
            telstra.addDuty("Provided administrative support to JIRA/Confluence ");
            elihu.addJob(telstra);

            Experience delConde = new Experience("Del Conde", "2015-2016", "General Manager");
            delConde.addDuty("Developed the company from start for the export and import of bananas from México to the United States");
            delConde.addDuty("Conducted the sales and revised the purchases of bananas among the material needed to pack and transport the product");
            delConde.addDuty("Conducted the negotiation with freight companies, and coordinated the general logistics with clients and custom brokers to cross the border");
            delConde.addDuty("Arranged and coordinated the payments to custom brokers, growers and materials for packing");
            delConde.addDuty("Structured the formula to calculate price needed for purchase and for sale to obtain the profit expected");
            delConde.addDuty("Led the negotiations with prospect clients to create a long-term business relationship ");
            elihu.addJob(delConde);

            Experience freelance = new Experience("Freelance", "2013-2016", "SoftRestaurant Programmer/Consultant");
            freelance.addDuty("Started the distribution for KSD and programming for KSD and SoftRestaurant for different kind of restaurants to help them improve their inventory, sales, business analytics and purchases control");
            freelance.addDuty("Developed clients over the city to either make a business proposal to install the software in their restaurants or provide the service of programming and consulting in the case that the restaurant has the software already");
            freelance.addDuty("Structured their work flow to standardize and improve their efficiency on the recipes for a better controlled inventory and more standardized product among the different branches");
            elihu.addJob(freelance);

            Experience comako = new Experience("Comako International", "2013", "General Coordinator");
            comako.addDuty("Coordinated a project of exporting fresh produce to the United States");
            comako.addDuty("Coordinated the construction of the packaging factory to pack the fresh produce under the brand of the company and prepare the shipment");
            comako.addDuty("Led negotiations with farmers and growers to create a business relationship");
            comako.addDuty("Solved logistics and technical problems with the growers and farmers");
            comako.addDuty("Coordinated and defined the job duties of a team of people");
            elihu.addJob(comako);

            Experience johnDeere = new Experience("John Deere", "2011", "Quality Engineer Intern");
            johnDeere.addDuty("Built the new FMEAs (Failure Mode and Effect Analysis) for the new line of products");
            johnDeere.addDuty("Applied APQP (Advanced Product Quality Planning) for the new line of products");
            johnDeere.addDuty("Learned the importance of quality controls, 5s, Root Cause analysis and 4Ds methodology ");
            elihu.addJob(johnDeere);

            Experience vikeda = new Experience("Vikeda", "2011", "Production Engineer Intern");
            vikeda.addDuty("Headed the line of production flow analysis");
            vikeda.addDuty("Applied the theory of restrictions to maximize production flow");
            vikeda.addDuty("Created the production line manuals for the operators");
            vikeda.addDuty("Created the factory and production layout to re-allocate machines to increase production flow");
            elihu.addJob(vikeda);

            elihu.addExtraInfo("#1 National Ranked 2013 Ironman");
            elihu.addExtraInfo("Top 50 National Ranked in Tennis Junior Tour");
            elihu.addExtraInfo("2x Full Ironman Finisher");
            elihu.addExtraInfo("I just love doing sports overall!");

            return elihu;
        }
        else if (who.equalsIgnoreCase("Amanda")){
           Profile amanda = new Profile("Amanda", "Tombolato", 21, "amandatombolato@gmail.com", "0416676803");

           Experience jredmond = new Experience("Julie Redmond Accounting and Taxation Services", "2018", "Administrator");
           jredmond.addDuty("Create tax returns");
           jredmond.addDuty("Bookkeeping on BGL software");
           jredmond.addDuty("Communicate with clients via email, phone, and letters");
           jredmond.addDuty("Liaise with the Australian Tax Office and ASIC");
           jredmond.addDuty("Courier financial documents to auditors and accountants");
           jredmond.addDuty("Navigate Xero, MYOB, ASIC and Quickbooks");
           jredmond.addDuty("Managed website using Weebly");

           Experience campion = new Experience("Campion Education", "2014-2016", "Customer Service");
           campion.addDuty("Managed the checkout");
           campion.addDuty("Assisted customers with finding books and stationery");
           campion.addDuty("Managed the warehouse");
           campion.addDuty("Packaged books and other post");
           campion.addDuty("Navigated the Bookmaster Pro software");
           campion.addDuty("Restocked shelves");

           Experience kreate = new Experience("Kreate", "2017", "Brand Ambassador");
           kreate.addDuty("Represented brands such as Samsung, Jaguar, Specsavers, Sylvanian Families");

           amanda.addJob(jredmond);
           amanda.addJob(kreate);
           amanda.addJob(campion);

           Education mgc = new Education("Melbourne Girls' College", "High School", "Melbourne, Australia", "2010", "2015");
           Education latrobe = new Education("La Trobe University", "Bachelor of Science in Information Technology", "Melbourne, Australia", "2017", "2019");
           amanda.addEducation(latrobe);
           amanda.addEducation(mgc);

           Experience photo = new Experience("La Trobe Tennis Team", "2017 - present", "Photographer");
           photo.addDuty("Took photos for promotional material, such as for their website and social media platforms");
           amanda.addVolunteering(photo);

           Experience church = new Experience("St Martin Of Tours Parish", "2007 - present", "Altar Server and Commentator");
           church.addDuty("Acted as Altar Server for weekly masses");
           church.addDuty("Commentator for weekly mass");
           amanda.addVolunteering(church);

           Language french = new Language("French","Advanced");
           Language english = new Language("English","Native");
           Language spanish = new Language("Spanish","Basic");
           Language italian = new Language("Italian","Basic");

           amanda.addLanguage(english);
           amanda.addLanguage(french);
           amanda.addLanguage(spanish);
           amanda.addLanguage(italian);

           amanda.addTechnology("SQL");
           amanda.addTechnology("Java");
           amanda.addTechnology("HTML/CSS/JavaScript");
           amanda.addTechnology("Microsoft Office");
           amanda.addTechnology("Tax Assist");
           amanda.addTechnology("BGL");
           amanda.addTechnology("Xero");

           amanda.addExtraInfo("Certificate of Completion for H&R Block Tax Course");
           amanda.addExtraInfo("Kwong Lee Dow Young Scholar Melbourne University");
           amanda.addExtraInfo("Member of Toastmasters since 2016");
           amanda.addExtraInfo("AMEB Grade 4 Clarinet");
           amanda.addExtraInfo("Number 1 Section 1 NEJTA Tennis Competition");


        return amanda;

        }
        else {
            return null;
        }
    }

}
