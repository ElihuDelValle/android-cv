package com.elihu.myapplication;

public class Education implements java.io.Serializable {
    private String institution;
    private String degree;
    private String location;
    private String fromYear;
    private String toYear;

    public Education(String institution, String degree, String location, String fromYear, String toYear) {
        this.institution = institution;
        this.degree = degree;
        this.location = location;
        this.fromYear = fromYear;
        this.toYear = toYear;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public String getDegree() {
        return degree;
    }

    public void setDegree(String degree) {
        this.degree = degree;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getFromYear() {
        return fromYear;
    }

    public void setFromYear(String fromYear) {
        this.fromYear = fromYear;
    }

    public String getToYear() {
        return toYear;
    }

    public void setToYear(String toYear) {
        this.toYear = toYear;
    }

    public String toString() {
        String string = "INSTITUTION: " + institution + "\n\n" +
                "DEGREE: " + degree + "\n\n" +
                "LOCATION: " + location + "\n\n" +
                "START YEAR: " + fromYear + "\n\n" +
                "END YEAR: " + toYear + "\n\n";

        return string;
    }

}
