package com.elihu.myapplication;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;


public class Contact_Screen extends AppCompatActivity implements View.OnClickListener {

    Profile profile;
    TextView nameTextView;
    TextView mobileTextView;
    TextView emailTextView;
    Button emailButton;
    Button mobileButton;
    Button exitContactButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact_screen);
        profile = (Profile) getIntent().getSerializableExtra("profile");
        nameTextView = findViewById(R.id.nameTextView);
        mobileTextView = findViewById(R.id.mobileTextView);
        emailTextView = findViewById(R.id.emailTextView);
        emailButton = findViewById(R.id.emailButton);
        mobileButton = findViewById(R.id.mobileButton);
        exitContactButton = findViewById(R.id.exitContactButton);

        nameTextView.setText(profile.getFullName());
        mobileTextView.setText(profile.getMobile());
        emailTextView.setText(profile.getEmail());

        emailButton.setOnClickListener(this);
        mobileButton.setOnClickListener(this);
        exitContactButton.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.mobileButton) {
            Intent phoneIntent = new Intent(Intent.ACTION_CALL);
            phoneIntent.setData(Uri.parse("tel:" + profile.getMobile()));
            if (ContextCompat.checkSelfPermission(Contact_Screen.this, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(Contact_Screen.this, new String[]{Manifest.permission.CALL_PHONE}, RequestCode.GRANTED);
            } else {
                startActivity(phoneIntent);
            }
        } else if (view.getId() == R.id.emailButton) {
            Intent mailIntent = new Intent(Intent.ACTION_VIEW);
            Uri data = Uri.parse("mailto:?subject=" + "I want to hire you!" + "&body=" + "Write me something! " + "&to=" + profile.getEmail());
            mailIntent.setData(data);
            startActivity(Intent.createChooser(mailIntent, "Send Email"));
        } else if (view.getId() == R.id.exitContactButton) {
            Intent exitIntent = new Intent(this, Entry_Screen.class);
            startActivity(exitIntent);
        }
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RequestCode.GRANTED: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

    class RequestCode {
        static final int GRANTED = 100;
    }

}

