package com.elihu.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;

public class AboutMe_Screen extends AppCompatActivity implements View.OnClickListener {

    private Profile profile;
    Button backButton;
    ListView listView;
    ArrayList<String> extraInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //Test Comment
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_me_screen);
        backButton = findViewById(R.id.exitExtraInfoButton);
        backButton.setOnClickListener(this);
        profile = (Profile) getIntent().getSerializableExtra("profile");
        extraInfo = profile.getExtraInfo();
        backButton = findViewById(R.id.exitExtraInfoButton);
        ListView listV = findViewById(R.id.extraInfoListView);
        final ArrayAdapter<String> adapter = new ArrayAdapter<>(listV.getContext(), android.R.layout.simple_list_item_1, extraInfo);
        listV.setAdapter(adapter);
        listView = listV;

    }

    public void onClick(View view) {
        if (view.getId() == R.id.exitExtraInfoButton) {
            Intent intent = new Intent(this, Entry_Screen.class);
            startActivity(intent);
        }
    }
}
