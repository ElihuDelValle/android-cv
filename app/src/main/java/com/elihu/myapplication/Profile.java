package com.elihu.myapplication;

import java.util.ArrayList;

public class Profile implements java.io.Serializable {

    private String firstName;
    private String surName;
    private String fullName;
    private int age;
    private String email;
    private ArrayList<Experience> experiences;
    private String mobile;
    private ArrayList<Education> education;
    private ArrayList<String> technologies;
    private ArrayList<Experience> volunteering;
    private ArrayList<Language> languages;
    private ArrayList<Referee> referees;
    private ArrayList<String> extraInfo;

    public Profile(String firstName, String surName, int age, String email, String mobile) {
        this.firstName = firstName;
        this.surName = surName;
        this.age = age;
        this.email = email;
        this.mobile = mobile;
        fullName = firstName + " " + surName;
        experiences = new ArrayList<>();
        education = new ArrayList<>();
        technologies = new ArrayList<>();
        volunteering = new ArrayList<>();
        languages = new ArrayList<>();
        referees = new ArrayList<>();
        extraInfo = new ArrayList<>();
    }

    public void addExtraInfo(String info) {
        extraInfo.add(info);
    }

    public ArrayList<String> getExtraInfo() {
        return extraInfo;
    }

    public void addReferee(Referee referee) {
        referees.add(referee);
    }

    public ArrayList<Referee> getReferees() {
        return referees;
    }

    public ArrayList<Experience> getVolunteering() {
        return volunteering;
    }

    public ArrayList<Language> getLanguages() {
        return languages;
    }

    public void addLanguage(Language language) {
        languages.add(language);
    }

    public void addVolunteering(Experience experience) {
        volunteering.add(experience);
    }

    public void addJob(Experience experience) {
        experiences.add(experience);
    }

    public void addEducation(Education educationLocal) {
        education.add(educationLocal);
    }

    public void addTechnology(String technology) {
        technologies.add(technology);
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getSurName() {
        return surName;
    }

    public void setSurName(String surName) {
        this.surName = surName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public ArrayList<Experience> getExperiences() {
        return experiences;
    }

    public void setExperiences(ArrayList<Experience> experiences) {
        this.experiences = experiences;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public ArrayList<Education> getEducation() {
        return education;
    }

    public void setEducation(ArrayList<Education> education) {
        this.education = education;
    }

    public ArrayList<String> getTechnologies() {
        return technologies;
    }

    public void setTechnologies(ArrayList<String> technologies) {
        this.technologies = technologies;
    }
}
