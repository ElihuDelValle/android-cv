package com.elihu.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class Education_Screen extends AppCompatActivity implements View.OnClickListener {

    private Profile profile;
    Button backButton;
    ListView listView;
    ArrayList<Education> educations;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.education_screen);
        backButton = findViewById(R.id.exitEducationButton);
        backButton.setOnClickListener(this);
        profile = (Profile) getIntent().getSerializableExtra("profile");
        educations = profile.getEducation();

        ArrayList<String> educationsString = new ArrayList<>();
        for (Education edu : educations) {
            educationsString.add(edu.toString());
        }

        backButton = findViewById(R.id.exitExperienceButton);
        ListView listV = findViewById(R.id.educationListView);
        CustomAdapter customAdapter = new CustomAdapter();
        listV.setAdapter(customAdapter);
        //final ArrayAdapter<String> adapter = new ArrayAdapter<>(listV.getContext(), android.R.layout.simple_list_item_1, educationsString);
        //listV.setAdapter(adapter);
        //listView = listV;

    }

    public void onClick(View view) {
        if (view.getId() == R.id.exitEducationButton) {
            Intent intent = new Intent(this, Entry_Screen.class);
            startActivity(intent);
        }
    }

    class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return educations.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.custom_layout_education, null);
            TextView institutionTextView = view.findViewById(R.id.institutionTextView);
            TextView degreeTextView = view.findViewById(R.id.degreeTextView);
            TextView yearTextView = view.findViewById(R.id.yearEducationTextView);
            TextView locationTextView = view.findViewById(R.id.locationTextView);

            institutionTextView.setText(educations.get(i).getInstitution());
            degreeTextView.setText(educations.get(i).getDegree());
            yearTextView.setText(educations.get(i).getFromYear() + "-" + educations.get(i).getToYear());
            locationTextView.setText(educations.get(i).getLocation());

            return view;
        }
    }
}
