package com.elihu.myapplication;

public class Language implements java.io.Serializable {

    private String nameOfLanguage;
    private String proficiency;

    public Language(String nameOfLanguage, String proficiency) {
        this.nameOfLanguage = nameOfLanguage;
        this.proficiency = proficiency;
    }

    public String getNameOfLanguage() {
        return nameOfLanguage;
    }

    public void setNameOfLanguage(String nameOfLanguage) {
        this.nameOfLanguage = nameOfLanguage;
    }

    public String getProficiency() {
        return proficiency;
    }

    public void setProficiency(String proficiency) {
        this.proficiency = proficiency;
    }
}
