package com.elihu.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Entry_Screen extends AppCompatActivity implements View.OnClickListener {

    Profile profile;
    TextView name;
    Button workButton;
    Button educationButton;
    Button contactButton;
    Button aboutMeButton;
    Button volunteeringButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entry_screen);
        workButton = findViewById(R.id.workButton);
        name = findViewById(R.id.nameTextView);
        educationButton = findViewById(R.id.educationButton);
        contactButton = findViewById(R.id.contactButton);
        aboutMeButton = findViewById(R.id.aboutMeButton);
        volunteeringButton = findViewById(R.id.volunteeringButton);

        profile = LoadData.LoadData("Elihu");
        name.setText(profile.getFirstName());

        workButton.setOnClickListener(this);
        educationButton.setOnClickListener(this);
        contactButton.setOnClickListener(this);
        aboutMeButton.setOnClickListener(this);
        volunteeringButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.contactButton) {
            Intent intent = new Intent(this, Contact_Screen.class);
            intent.putExtra("profile", profile);
            startActivity(intent);
        } else if (view.getId() == R.id.aboutMeButton) {
            Intent intent = new Intent(this, AboutMe_Screen.class);
            intent.putExtra("profile", profile);
            startActivity(intent);
        } else if (view.getId() == R.id.workButton) {
            Intent intent = new Intent(this, Experience_Screen.class);
            intent.putExtra("profile", profile);
            startActivity(intent);
        } else if (view.getId() == R.id.educationButton) {
            Intent intent = new Intent(this, Education_Screen.class);
            intent.putExtra("profile", profile);
            startActivity(intent);
        } else if (view.getId() == R.id.volunteeringButton) {
            Intent intent = new Intent(this, Volunteering_Screen.class);
            intent.putExtra("profile", profile);
            startActivity(intent);
        }
    }
}
