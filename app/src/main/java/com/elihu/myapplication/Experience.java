package com.elihu.myapplication;

import java.util.ArrayList;

public class Experience implements java.io.Serializable {

    private String company;
    private ArrayList<String> duties;
    private String position;
    private String year;

    public Experience(String company, String year, String position) {
        this.company = company;
        this.year = year;
        this.position = position;
        duties = new ArrayList<>();
    }

    public void addDuty(String duty) {
        duties.add(duty);
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getDutiesString() {

        String string = "";
        for (String duty : duties) {
            string += "\n" + duty + "\n";
        }

        return string;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String toString() {
        String string = "COMPANY: " + company + "\n\n" +
                "POSITION: " + position + "\n\n" +
                "YEAR: " + year + "\n\n";
        for (String duty : duties) {
            string += "\n" + duty + "\n";
        }

        return string;
    }
}
