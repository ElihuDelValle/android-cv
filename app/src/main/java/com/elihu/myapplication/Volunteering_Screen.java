package com.elihu.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class Volunteering_Screen extends AppCompatActivity implements View.OnClickListener {

    private Profile profile;
    Button backButton;
    ArrayList<Experience> volunteering;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.experience_screen);
        backButton = findViewById(R.id.exitExperienceButton);
        backButton.setOnClickListener(this);
        profile = (Profile) getIntent().getSerializableExtra("profile");
        volunteering = profile.getVolunteering();

        backButton = findViewById(R.id.exitExperienceButton);
        ListView listV = findViewById(R.id.experienceListView);

        CustomAdapter customAdapter = new CustomAdapter();
        listV.setAdapter(customAdapter);


    }

    public void onClick(View view) {
        if (view.getId() == R.id.exitExperienceButton) {
            Intent intent = new Intent(this, Entry_Screen.class);
            startActivity(intent);
        }
    }

    class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return volunteering.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        public View getView(int i, View view, ViewGroup viewGroup) {
            view = getLayoutInflater().inflate(R.layout.custom_layout_experience, null);
            TextView titleTextView = view.findViewById(R.id.titleTextView);
            TextView companyTextView = view.findViewById(R.id.companyNameTextView);
            TextView yearTextView = view.findViewById(R.id.yearTextView);
            TextView dutiesTextView = view.findViewById(R.id.dutiesTextView);

            titleTextView.setText(volunteering.get(i).getPosition());
            companyTextView.setText(volunteering.get(i).getCompany());
            yearTextView.setText(volunteering.get(i).getYear());
            dutiesTextView.setText(volunteering.get(i).getDutiesString());

            return view;
        }
    }
}
